﻿using System.Windows.Controls;

namespace EVS_Support_Tool.CI_Retriever
{
    class Queries
    {
        public string GetConnectString()
        {
            return "Data Source=10.88.29.17;Initial Catalog=EVS45;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        }


        public string GetClientName(TextBox client)
        {
            string clientName;

            clientName = @"select 
                    name
                    from client
                    where name = '" + (client.Text.Trim()) + "'";

            return clientName;
        }


        public string GetLocale(TextBox client)
        {
            string locale;

            locale = @"SELECT DISTINCT
                             cil.name
                            FROM
                             PortalPage AS pp
                             LEFT OUTER JOIN Client cli ON pp.ClientId = cli.ClientId
                             LEFT JOIN PortalPageType ppt ON ppt.PortalPageTypeId = pp.PortalPageTypeId
                             LEFT JOIN ServiceLevel sl ON sl.ServiceLevelId = pp.ServiceLevelId
                             LEFT JOIN dbo.CandidateInviteLocale cil ON cil.CandidateInviteLocaleId = pp.CandidateInviteLocaleId
                             LEFT JOIN ContractServiceLevel csl ON sl.ServiceLevelId = csl.ServiceLevelId
                             LEFT JOIN Contract con ON csl.ContractId = con.ContractId
                            WHERE
                              cli.name = '" + (client.Text.Trim()) +
                              "' AND(con.IsActive = 1 OR con.IsActive IS NULL) AND cil.Name IS NOT NULL";

            return locale;
        }


        public string GetRowsCount(TextBox client)
        {
            string rows;

            rows = @"select 
                                count(*)
                                from
	                                PortalPage as pp
	                                left outer join Client cli on pp.ClientId = cli.ClientId
	                                left join PortalPageType ppt on ppt.PortalPageTypeId = pp.PortalPageTypeId
	                                left join ServiceLevel sl on sl.ServiceLevelId = pp.ServiceLevelId
	                                left join dbo.CandidateInviteLocale cil on cil.CandidateInviteLocaleId = pp.CandidateInviteLocaleId
	                                left join ContractServiceLevel csl on sl.ServiceLevelId = csl.ServiceLevelId
	                                left join Contract con on csl.ContractId = con.ContractId
                                where
	                                cli.name = '" + (client.Text.Trim()) + "' and (con.IsActive = 1 or con.IsActive IS NULL)";

            return rows;
        }



        public string GetDefaultWordingsRelatedHTMLData(TextBox client, string locale)
        {
            string defaultWordingsAdditional;

            defaultWordingsAdditional = @"select
	                                ppt.name,
	                                pp.pagenumber,
	                                pp.title,
	                                pp.html
                                from
	                                PortalPage as pp
	                                left outer join Client cli on pp.ClientId = cli.ClientId
	                                left join PortalPageType ppt on ppt.PortalPageTypeId = pp.PortalPageTypeId
	                                left join ServiceLevel sl on sl.ServiceLevelId = pp.ServiceLevelId
	                                left join dbo.CandidateInviteLocale cil on cil.CandidateInviteLocaleId = pp.CandidateInviteLocaleId
	                                left join ContractServiceLevel csl on sl.ServiceLevelId = csl.ServiceLevelId
	                                left join Contract con on csl.ContractId = con.ContractId
                                where
	                                cli.name = '" + (client.Text.Trim()) + @"'
	                                and (con.IsActive = 1 or con.IsActive IS NULL)
	                                and sl.name is null
	                                and cil.Name = '" + locale + @"'

                                order by
	                                cli.name,
	                                sl.name,
	                                ppt.name,
	                                cil.name,
	                                pp.pagenumber,
	                                pp.PortalPageId";

            return defaultWordingsAdditional;
        }


        public string GetCustomSLAWordingsRelatedHTMLData(TextBox client, string locale)
        {
            string customSLAWordingsAdditional;

            customSLAWordingsAdditional = @"select distinct
	                            sl.Name
                            from

	                            PortalPage as pp
	                            left outer join Client cli on pp.ClientId = cli.ClientId
	                            left join PortalPageType ppt on ppt.PortalPageTypeId = pp.PortalPageTypeId
	                            left join ServiceLevel sl on sl.ServiceLevelId = pp.ServiceLevelId
	                            left join dbo.CandidateInviteLocale cil on cil.CandidateInviteLocaleId = pp.CandidateInviteLocaleId
	                            left join ContractServiceLevel csl on sl.ServiceLevelId = csl.ServiceLevelId
	                            left join Contract con on csl.ContractId = con.ContractId
                            where
	                            cli.name = '" + (client.Text.Trim()) + @"'
	                            and (con.IsActive = 1 or con.IsActive IS NULL)
	                            and sl.name is not null
	                            and cil.Name = '" + locale + @"'";

            return customSLAWordingsAdditional;
        }


        public string GetDefaultWordingsBodyHTML(TextBox client, string locale, string pptype)
        {
            string nonSLWordingsHeadersBody;

            nonSLWordingsHeadersBody = @"select
	                        cli.name,
	                        ppt.name ,
	                        pp.pagenumber,
	                        cil.name,
	                        pp.title,
	                        pp.html

                        from

	                        PortalPage as pp
	                        left outer join Client cli on pp.ClientId = cli.ClientId
	                        left join PortalPageType ppt on ppt.PortalPageTypeId = pp.PortalPageTypeId
	                        left join ServiceLevel sl on sl.ServiceLevelId = pp.ServiceLevelId
	                        left join dbo.CandidateInviteLocale cil on cil.CandidateInviteLocaleId = pp.CandidateInviteLocaleId
	                        left join ContractServiceLevel csl on sl.ServiceLevelId = csl.ServiceLevelId
	                        left join Contract con on csl.ContractId = con.ContractId
                        where
	                        cli.name = '" + (client.Text.Trim()) + @"'
	                        and (con.IsActive = 1 or con.IsActive IS NULL)
	                        and sl.name is null

                        and cil.name = '" + locale + @"'
	                        and ppt.name = '" + pptype + "'";

            return nonSLWordingsHeadersBody;
        }


        public string GetCustomSLAWordingsBodyHTML(TextBox client, string locale, string sl)
        {
            string nonSLWordingsHeadersBody;

            nonSLWordingsHeadersBody = @"select
	                        sl.name,
	                        ppt.name ,
	                        pp.pagenumber,
	                        cil.name,
	                        pp.title,
	                        pp.html

                        from

	                        PortalPage as pp
	                        left outer join Client cli on pp.ClientId = cli.ClientId
	                        left join PortalPageType ppt on ppt.PortalPageTypeId = pp.PortalPageTypeId
	                        left join ServiceLevel sl on sl.ServiceLevelId = pp.ServiceLevelId
	                        left join dbo.CandidateInviteLocale cil on cil.CandidateInviteLocaleId = pp.CandidateInviteLocaleId
	                        left join ContractServiceLevel csl on sl.ServiceLevelId = csl.ServiceLevelId
	                        left join Contract con on csl.ContractId = con.ContractId
                        where
	                        cli.name = '" + (client.Text.Trim()) + @"'
	                        and (con.IsActive = 1 or con.IsActive IS NULL)
	                        and sl.name is not null

                        and cil.name = '" + locale + @"'
	                        and sl.name = '" + sl + "'";

            return nonSLWordingsHeadersBody;
        }
    }
}
