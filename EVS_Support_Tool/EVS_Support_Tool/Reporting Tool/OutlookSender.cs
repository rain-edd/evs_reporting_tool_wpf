﻿using System;
using System.IO;
using System.Text;
using System.Windows.Controls;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace EVS_Support_Tool.Reporting_Tool
{
    class OutlookSender
    {
        public void CreateOutlookEmail(ListBox lbQueries, bool draft)
        {   
            // Class Instances
            ConfigTagOutlookSendTo sendTo = new ConfigTagOutlookSendTo();
            ConfigTagOutlookEmailBody body = new ConfigTagOutlookEmailBody();   

            Outlook.Application outlookApp = new Outlook.Application();
            Outlook.MailItem mailItem = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);

            mailItem.Subject = ReportCreator.ReportSubject_static;
            mailItem.To = sendTo.ReadTagContent(lbQueries.SelectedValue.ToString());
            mailItem.Importance = Outlook.OlImportance.olImportanceLow;

            // Attachments.Add can only take a string with the file name or another Outlook item (MailItem, XContactItem, etc.). We take string.
            mailItem.Attachments.Add(ReportCreator.ReportPathFilenameExtension_static.ToString(), Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);

            mailItem.HTMLBody = $"<body style='font-family:arial;font-size:10pt'>Hello,<br><br>{body.ReadTagContent(lbQueries.SelectedValue.ToString())}<br><br></body>{GetOutlookSignatureForCurrentLoggedInOutlookAccount()}";
            mailItem.Save();

            if(draft == false)
            {
                mailItem.Send();
            }
        }

        // When report has zero records, only create warning email that we have empty results
        public void CreateOutlookEmailWithZeroResults(ListBox lbQueries, bool draft)
        {
            // Class Instances
            ConfigTagOutlookSendTo sendTo = new ConfigTagOutlookSendTo();
            ConfigTagOutlookEmailBody body = new ConfigTagOutlookEmailBody();

            Outlook.Application outlookApp = new Outlook.Application();
            Outlook.MailItem mailItem = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);

            mailItem.Subject = $"{ReportCreator.ReportSubject_static}  :  NO RECORDS FOUND";
            mailItem.To = sendTo.ReadTagContent(lbQueries.SelectedValue.ToString());
            mailItem.Importance = Outlook.OlImportance.olImportanceLow; 

            mailItem.HTMLBody = $"<body style='font-family:arial;font-size:10pt'>Hello,<br><br>Please be informed that there are no records found matching for selected dates.<br><br></body>{GetOutlookSignatureForCurrentLoggedInOutlookAccount()}";
            mailItem.Save();

            if (draft == false)
            {
                mailItem.Send();
            }
        }

        // From Stackoverflow: Signature adding in outlook
        private string GetOutlookSignatureForCurrentLoggedInOutlookAccount()
        {
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signatures";
            string outlookSignature = string.Empty;
            DirectoryInfo diInfo = new DirectoryInfo(appDataDir);
            if (diInfo.Exists)
            {
                FileInfo[] fiSignature = diInfo.GetFiles("*.htm");
                if (fiSignature.Length > 0)
                {
                    StreamReader sr = new StreamReader(fiSignature[0].FullName, Encoding.Default);
                    outlookSignature = sr.ReadToEnd();

                    if (!string.IsNullOrEmpty(outlookSignature))
                    {
                        string fileName = fiSignature[0].Name.Replace(fiSignature[0].Extension, string.Empty);
                        outlookSignature = outlookSignature.Replace(fileName + "_files/", appDataDir + "/" + fileName + "_files/");
                    }
                }
            }

            return outlookSignature;
        }
    }
}
