﻿using BespokeFusion;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace EVS_Support_Tool.Reporting_Tool
{
    class LoggerMessage
    {
        private string _message;

        public string MessageQueriesLoaded()
        {
            _message = "- queries loaded: \t\t\t\tOK\n";

            return _message;
        }

        public string MessageJSONRecordNotCreated()
        {
            _message = "- JSON record created: \t\t\t\tFAILED\t\t (report name already exists)\n";

            return _message;
        }

        public string MessageNewReportFileCreatedWithOneSheet()
        {
            _message = "- report file (1 sheet) created: \t\t\t\tOK\n";

            return _message;
        }

        public string MessageNewReportFileCreatedWithTwoSheets()
        {
            _message = "- report file (2 sheets) created: \t\t\t\tOK\n";

            return _message;
        }

        public string MessageNewReportFileCreatedFailedAlreadyExists()
        {
            _message = "- report file created: \t\t\t\tFAILED\t\t (file already exists)\n";

            return _message;
        }

        public string MessageReportDeleted()
        {
            _message = "- report deleted: \t\t\t\tOK\n";

            return _message;
        }

        public string MessageReportUpdated()
        {
            _message = "- report updated: \t\t\t\tOK\n";

            return _message;
        }


        public void MessageQueryExecutingWithRunningDots(Dispatcher dispatcher, TextBox tb)
        {
            dispatcher.BeginInvoke((Action)(() => tb.AppendText("\nExecuting ")));

            for (int i = 0; i < 4; i++)
            {
                dispatcher.BeginInvoke((Action)(() => tb.AppendText(".")));
                Thread.Sleep(250);
            }
        }


        public string MessageQueryExecutionSheet1FinishedOK()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tcompleted: sheet 1: \t\tOK    rows: {ReportCreator.ReportTotalRowsSheet1_static}";

            return _message;
        }

        public string MessageQueryExecutionSheet2FinishedOK()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tcompleted: sheet 2: \t\tOK    rows: {ReportCreator.ReportTotalRowsSheet2_static}";

            return _message;
        }


        public string MessageQueryExecutionSheet1ZeroResults()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tcompleted: sheet 1: \t\tOK    zero results";

            return _message;
        }

        public string MessageQueryExecutionSheet2ZeroResults()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tcompleted: sheet 2: \t\tOK    zero results";

            return _message;
        }


        public string MessageQueryExecutionFailed()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tcompleted: \t\t\t\tFAILED\n";

            return _message;
        }

        public string MessageReportAddedToOutlookDraftsFolderOK()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tadded to Outlook drafts: \t\tOK\n";

            return _message;
        }

        public string MessageReportAddedToOutlookDraftsFolderFailed()
        {
            _message = $"\n- query {ReportCreator.ReportSubject_static.PadRight(60, ' ')} \t\t\t\tadded to Outlook drafts: \t\tFAILED\n";

            return _message;
        }
    }


    class PopupMessage
    {
        public void MessageBoxErrorPopup(string errorDescription)
        {
            var converter = new BrushConverter();
            var mainThemeColor = (Brush)converter.ConvertFromString("#FF5C99D6");

            var msg = new CustomMaterialMessageBox
            {
                TxtMessage = { Text = errorDescription, Foreground = Brushes.Black },
                TxtTitle = { Text = "Warning", Foreground = Brushes.Black },
                BtnOk = { Content = "OK", Width = 70, Background = Brushes.Red },
                BtnCancel = { Visibility = Visibility.Collapsed },
                MainContentControl = { Background = Brushes.White },
                TitleBackgroundPanel = { Background = Brushes.Red },
                BtnCopyMessage = { Visibility = Visibility.Collapsed },
                BorderBrush = Brushes.Red
            };

            msg.BtnOk.Focus();
            msg.Show();
        }  


        public CustomMaterialMessageBox MessageBoxConfirmationPopup(string confirmDescription, CustomMaterialMessageBox clickOKCancel)
        {
            var converter = new BrushConverter();
            var mainThemeColor = (Brush)converter.ConvertFromString("#FF5C99D6");

            clickOKCancel = new CustomMaterialMessageBox
            {
                TxtMessage = { Text = confirmDescription, Foreground = Brushes.Black },
                TxtTitle = { Text = "Confirmation required", Foreground = Brushes.Black },
                BtnOk = { Content = "OK", Width = 70, Background = Brushes.Green },
                BtnCancel = { Visibility = Visibility.Visible, Background = mainThemeColor },
                MainContentControl = { Background = Brushes.White },
                TitleBackgroundPanel = { Background = mainThemeColor },
                BtnCopyMessage = { Visibility = Visibility.Collapsed },
                BorderBrush = mainThemeColor
            };

            clickOKCancel.BtnOk.Focus();
            clickOKCancel.Show();

            return clickOKCancel;
        }
    }



    /// <summary>
    /// Overriding the cursor when loading reports
    /// </summary>
    public class StackedCursorOverride : IDisposable
    {
        private readonly static Stack<Cursor> CursorStack;

        static StackedCursorOverride()
        {
            CursorStack = new Stack<Cursor>();
        }

        public StackedCursorOverride(Cursor cursor)
        {
            CursorStack.Push(cursor);
            Mouse.OverrideCursor = cursor;
        }

        public void Dispose()
        {
            var previousCursor = CursorStack.Pop();
            if (CursorStack.Count == 0)
            {
                Mouse.OverrideCursor = null;
                return;
            }

            // if next cursor is the same as the one we just popped, don't change the override
            if ((CursorStack.Count > 0) && (CursorStack.Peek() != previousCursor))
                Mouse.OverrideCursor = CursorStack.Peek();
        }
    }
}

