﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace EVS_Support_Tool.Reporting_Tool
{
    /// <summary>
    /// 
    /// </summary>
    class ScheduledForToday
    {
        public List<string> CalculateReportsRequiredForToday(ListBox lb)
        {
            // Class Instances
            Directories directories = new Directories();

            List<string> listOfAllReports;
            List<string> listOfReportsScheduledForToday;

            DateTime dateToday;
            DateTime dateStartOfCurrentMonth;


            dateToday = DateTime.Now.Date;
            dateStartOfCurrentMonth = new DateTime(dateToday.Year, dateToday.Month, 1);


            // If the fist day of current month is Sun or Sat then add some days so it will be visible on 2nd or 3rd day as on Monday
            if (dateStartOfCurrentMonth.DayOfWeek == DayOfWeek.Saturday)
            {
                dateStartOfCurrentMonth = dateStartOfCurrentMonth.AddDays(2);
            }

            else if (dateStartOfCurrentMonth.DayOfWeek == DayOfWeek.Sunday)
            {
                dateStartOfCurrentMonth = dateStartOfCurrentMonth.AddDays(1);
            }


            // Loading reports into listbox
            directories.LoadReportsIntoListboxFromJSONUniqueNames(lb);


            // Making two copies of all reports we loaded into List
            listOfAllReports = lb.Items.Cast<string>().ToList();
            listOfReportsScheduledForToday = lb.Items.Cast<string>().ToList();

            // Looping through one List and Removing reports from another list, which we will use further
            for (int i = 0; i < listOfAllReports.Count; i++)
            {
                // Class Instances
                ConfigTagType configTagType = new ConfigTagType();
                ConfigTagSendOnDay day = new ConfigTagSendOnDay();

                string report;
                string type;
                string daysSendOnString;

                report = listOfAllReports[i];
                type = configTagType.ReadTagContent(listOfAllReports[i]);
                daysSendOnString = day.ReadTagContent(report);

                // Remove weekly scripts if today IS NOT Monday
                if (dateToday.DayOfWeek != DayOfWeek.Monday && type == "weekly")
                {
                    listOfReportsScheduledForToday.Remove(report);
                }

                // Remove monthly scripts if today IS NOT first day of a current month
                if (dateToday != dateStartOfCurrentMonth && type == "monthly")
                {
                    listOfReportsScheduledForToday.Remove(report);
                }

                // Additional check for DaysSendOnString for daily reports
                if (daysSendOnString != "" && type == "daily")
                {
                    List<string> listOfParsedDaysSendOnString = new List<string>();
                    listOfParsedDaysSendOnString.Clear();

                    bool isRequiredToBeSendToday = false;

                    // Parse daysSendOn string into list
                    foreach (string str in daysSendOnString.Split(','))
                    {
                        listOfParsedDaysSendOnString.Add(str.Trim());
                    }

                    // Scroll through parsed daysSendOn list and remove report if this does not meet the conditions
                    foreach (var item in listOfParsedDaysSendOnString)
                    {
                        if (item == dateToday.DayOfWeek.ToString())
                        {
                            isRequiredToBeSendToday = true;
                        }                       
                    }

                    if (isRequiredToBeSendToday == false)
                    {
                        listOfReportsScheduledForToday.Remove(report);              
                    }
                }     
            }

            return listOfReportsScheduledForToday;
        }
    }
}
