﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace EVS_Support_Tool.Reporting_Tool
{
    /// <summary>
    /// Interaction logic for AllReports.xaml
    /// </summary>

    public partial class UserControl_Reports : UserControl
    {
        // Class Instance
        LoggerMessage logger = new LoggerMessage();

        private string _loggerText;

        private static BrushConverter _converter = new BrushConverter();
        private Brush mainThemeColor = (Brush)_converter.ConvertFromString("#FF2196F3");

        public UserControl_Reports()
        {
            // Class Instance
            LoggerMessage logger = new LoggerMessage();
            Directories directories = new Directories();

            InitializeComponent();

            StartupSettings();



            directories.LoadReportsIntoListboxFromJSONUniqueNames(lbQueries_REP);

            _loggerText = logger.MessageQueriesLoaded();
            textboxLogger_REP.Text = _loggerText;

            // Automatical visibility for reports only scheduled for today when entering reports list section
            checkboxForToday_REP.IsChecked = true;
        }

        public void ListboxQueries_REP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbQueries_REP.SelectedIndex != -1)
            {
                QueriesSelectionActions();
            }
        }

        private void DatePickerStart_REP_CalendarClosed(object sender, RoutedEventArgs e)
        {
            DatePickersCalendarDateChooseActions();
        }

        private void DatePickerEnd_REP_CalendarClosed(object sender, RoutedEventArgs e)
        {
            DatePickersCalendarDateChooseActions();
        }

        private void TextboxLogger_REP_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Always show the last line in textbox logger when text is being added
            if (textboxLogger_REP.LineCount != -1)
            {
                textboxLogger_REP.ScrollToLine(textboxLogger_REP.LineCount - 1);
            }
        }

        private void Sheet1Button_REP_Click(object sender, RoutedEventArgs e)
        {
            Sheet1ButtonSettings();
        }


        private void Sheet2Button_REP_Click(object sender, RoutedEventArgs e)
        {
            Sheet2ButtonSettings();
        }


        private async void ButtonExecuteAll_REP_Click(object sender, RoutedEventArgs e)
        {

            lbQueries_REP.SelectAll();

            await FileCreation();

            ScheduledForTodayCheckedActions();
        }
 
        private void ScheduledForToday_checked_REP(object sender, RoutedEventArgs e)
        {

            QueriesSelectionSettings();

            ScheduledForTodayCheckedActions();
        }

        private void ScheduledForToday_unchecked_REP(object sender, RoutedEventArgs e)
        {
            StartupSettings();

            AllReportsUncheckedkActions();
        }

        private async void ButtonCreate_REP_ClickAsync(object sender, RoutedEventArgs e)
        {
            try
            {
                await FileCreation();
            }


            catch
            {
                // "Finishing" message
                _loggerText = "";
                _loggerText += logger.MessageQueryExecutionFailed();
                textboxLogger_REP.Text += _loggerText;
            }

            StartupSettings();
        }



        private void StartupSettings()
        {
            sheet1Button_REP.Visibility = Visibility.Hidden;
            sheet2Button_REP.Visibility = Visibility.Hidden;

            tbQueriesBodySheet1_REP.Visibility = Visibility.Hidden;
            tbQueriesBodySheet2_REP.Visibility = Visibility.Hidden;

            tBlockSheets.Visibility = Visibility.Hidden;
            tBlockConnection.Visibility = Visibility.Hidden;
            tbReportConnection_REP.Visibility = Visibility.Hidden;

            buttonCreate_REP.IsEnabled = false;
            datePickerStart_REP.Visibility = Visibility.Hidden;
            datePickerEnd_REP.Visibility = Visibility.Hidden;

            buttonExecuteAll_REP.Visibility = Visibility.Hidden;      

            lbQueries_REP.SelectedIndex = -1;
        }

        private void QueriesSelectionSettings()
        {
            tbQueriesBodySheet1_REP.Text = "";
            tbQueriesBodySheet2_REP.Text = "";

            datePickerStart_REP.SelectedDate = null;
            datePickerEnd_REP.SelectedDate = null;

            sheet1Button_REP.Visibility = Visibility.Visible;
            sheet2Button_REP.Visibility = Visibility.Visible;

            sheet1Button_REP.BorderBrush = mainThemeColor;
            sheet2Button_REP.BorderBrush = Brushes.Transparent;

            tBlockSheets.Visibility = Visibility.Visible;
            tBlockConnection.Visibility = Visibility.Visible;

            buttonCreate_REP.IsEnabled = true;
            datePickerStart_REP.Visibility = Visibility.Hidden;
            datePickerEnd_REP.Visibility = Visibility.Hidden;

            tbQueriesBodySheet1_REP.Visibility = Visibility.Visible;
            tbQueriesBodySheet2_REP.Visibility = Visibility.Hidden;

            sheet2Button_REP.IsEnabled = false;
        }


        private async Task Task_MessageQueryExecutingWithRunningDots()
        {
            var thread = new Thread(MessageQueryExecutingWithRunningDots);
            thread.Start();

            await Task.Delay(330);
        }


        private void MessageQueryExecutingWithRunningDots()
        {
            Dispatcher.BeginInvoke((Action)(() => textboxLogger_REP.AppendText("\n- executing ")));

            for (int i = 0; i < 3; i++)
            {
                Dispatcher.BeginInvoke((Action)(() => textboxLogger_REP.AppendText("  .")));
                Thread.Sleep(150);
            }
        }


        private async Task FileCreation()
        {
            // "Starting" message
            Task task = Task_MessageQueryExecutingWithRunningDots();
            await task;

            textboxLogger_REP.Text += "\n"; // Add new line after phrase "executing ..."

            using (new StackedCursorOverride(Cursors.Wait)) // Cursors change
            {
                List<object> listOfReportsSelectedForExecution = new List<object>();


                for (int i = 0; i < lbQueries_REP.SelectedItems.Count; i++)
                {
                    listOfReportsSelectedForExecution.Add(lbQueries_REP.SelectedItems[i]);
                }

                for (int i = 0; i < listOfReportsSelectedForExecution.Count; i++)
                {
                    // Class Instances
                    SupportingMethods supportingMethods = new SupportingMethods();
                    ReportCreator reportCreator = new ReportCreator();
                    OutlookSender outlookSender = new OutlookSender();

                    bool sheet2Required;
                    int reportIndex;
                    string selectedReport;

                    // Get index of currently selected report and make this script selected accordingly
                    // so the tool could read date tags and etc for every selected scripts if more than one is selected
                    reportIndex = lbQueries_REP.Items.IndexOf(listOfReportsSelectedForExecution[i]);
                    lbQueries_REP.SelectedIndex = reportIndex;

                    selectedReport = lbQueries_REP.Items[reportIndex].ToString();

                    try 
                    {
                        reportCreator.ExecuteFileCreationMethods(
                            selectedReport, datePickerStart_REP, datePickerEnd_REP,
                            tbQueriesBodySheet1_REP, tbQueriesBodySheet2_REP);
                    }

                    catch (Exception e)
                    {
                        // Class Instance 
                        PopupMessage popup = new PopupMessage();
                        popup.MessageBoxErrorPopup("Error occured while creating a file...\n\n\n\n " + e);           

                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageQueryExecutionFailed();
                        textboxLogger_REP.Text += _loggerText;

                        //return;

                        continue;
                    }

                    sheet2Required = supportingMethods.Sheet2IsRequiredAfterSetupHasBeenDone(selectedReport);

                    // -1 : error code : file is not created
                    //  0 : error code : file created with 0 results from sql 
                    //  1 : error code : file created

                    if (ReportCreator.ReportTotalRowsSheet1_static == -1 || ReportCreator.ReportTotalRowsSheet2_static == -1)
                    {
                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageQueryExecutionFailed();
                        textboxLogger_REP.Text += _loggerText;

                        await Task.Delay(100);

                        return;
                    }

                    if (ReportCreator.ReportTotalRowsSheet1_static > 0)
                    {
                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageQueryExecutionSheet1FinishedOK();
                        textboxLogger_REP.Text += _loggerText;

                        await Task.Delay(100);
                    }

                    if (ReportCreator.ReportTotalRowsSheet2_static > 0 && sheet2Required)
                    {
                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageQueryExecutionSheet2FinishedOK();
                        textboxLogger_REP.Text += _loggerText;

                        await Task.Delay(100);
                    }

                    if (ReportCreator.ReportTotalRowsSheet1_static == 0)
                    {
                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageQueryExecutionSheet1ZeroResults();
                        textboxLogger_REP.Text += _loggerText;

                        await Task.Delay(100);
                    }

                    if (ReportCreator.ReportTotalRowsSheet2_static == 0 && sheet2Required)
                    {
                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageQueryExecutionSheet2ZeroResults();
                        textboxLogger_REP.Text += _loggerText;

                        await Task.Delay(100);
                    }


                    // Transfer generated report to Drafts in Outlook
                    try
                    {
                        if (ReportCreator.ReportTotalRowsSheet1_static == 0)
                        {
                            outlookSender.CreateOutlookEmailWithZeroResults(lbQueries_REP, (bool)checkboxAsDraft_REP.IsChecked);
                        }

                        else
                        {                            
                            outlookSender.CreateOutlookEmail(lbQueries_REP, (bool)checkboxAsDraft_REP.IsChecked);
                        }

                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageReportAddedToOutlookDraftsFolderOK();
                        textboxLogger_REP.Text += _loggerText;
                    }

                    catch
                    {
                        // "Finishing" message
                        _loggerText = "";
                        _loggerText += logger.MessageReportAddedToOutlookDraftsFolderFailed();
                        textboxLogger_REP.Text += _loggerText;
                    }
                }
            }
        }


        private void QueriesSelectionActions()
        {
            // Class Instances
            SupportingMethods supportingMethods = new SupportingMethods();
            ConfigTagConnection conn = new ConfigTagConnection();
            QueryDateTagsReplacer queryDateTagsReplacer = new QueryDateTagsReplacer();

            QueriesSelectionSettings();

            Console.WriteLine();
          

            // Represent connection value for this report in a textbox
            tbReportConnection_REP.Text = conn.ReadTagContent(lbQueries_REP.SelectedValue.ToString());
            tbReportConnection_REP.Visibility = Visibility.Visible;

            // Update date tags
            queryDateTagsReplacer.ReplaceSQLQueryDataTagsWhileReportCreation(lbQueries_REP.SelectedValue.ToString(),
                datePickerStart_REP, datePickerEnd_REP, tbQueriesBodySheet1_REP, tbQueriesBodySheet2_REP);


            // Make datepickers visible if we have Date Tags in the script
            if (datePickerStart_REP.Text != "")
            {
                datePickerStart_REP.Visibility = Visibility.Visible;
            }

            if (datePickerEnd_REP.Text != "")
            {
                datePickerEnd_REP.Visibility = Visibility.Visible;
            }


            // In case we have only 1 sheet at report, apply button actions
            bool sheet2Required = supportingMethods.Sheet2IsRequiredAfterSetupHasBeenDone(lbQueries_REP.SelectedValue.ToString());

            if (sheet2Required)
            {
                sheet2Button_REP.IsEnabled = true;
            }

            // In case we have 2 sheets at report, apply sheet 1 only needed actions
            else
            {
                Sheet1ButtonSettings();
            }
        }


        private void DatePickersCalendarDateChooseActions()
        {
            // Class Instances
            QueryDateTagsReplacer queryDateTagsReplacer = new QueryDateTagsReplacer();
            SupportingMethods supportingMethods = new SupportingMethods();
            ConfigTagSheet1SQL configTagSheet1SQL = new ConfigTagSheet1SQL();
            ConfigTagSheet2SQL configTagSheet2SQL = new ConfigTagSheet2SQL();

            string queryWithoutDateTagsReplacedSheet1;
            string queryWithDateTagsReplacedSheet1;

            string queryWithoutDateTagsReplacedSheet2;
            string queryWithDateTagsReplacedSheet2;

            bool calendarOpenedDateTaken = true;
            bool sheet2Required;

            sheet2Required = supportingMethods.Sheet2IsRequiredAfterSetupHasBeenDone(lbQueries_REP.SelectedValue.ToString());


            // Get the raw query with date tags, e.g. #StartDate# or #EndDate#
            queryWithoutDateTagsReplacedSheet1 = configTagSheet1SQL.ReadTagContent(lbQueries_REP.SelectedValue.ToString());

            // And update it
            queryWithDateTagsReplacedSheet1 = queryDateTagsReplacer.ReplaceDateTagsWithDateValues(
                  lbQueries_REP.SelectedValue.ToString(), calendarOpenedDateTaken,
                  queryWithoutDateTagsReplacedSheet1, datePickerStart_REP, datePickerEnd_REP);

            tbQueriesBodySheet1_REP.Text = queryWithDateTagsReplacedSheet1;


            // Create 2 sheet if required and do the same for 2 sheet
            if (sheet2Required == true)
            {
                // Get the raw query with date tags, e.g. #StartDate# or #EndDate#
                queryWithoutDateTagsReplacedSheet2 = configTagSheet2SQL.ReadTagContent(lbQueries_REP.SelectedValue.ToString());
                // And update it
                queryWithDateTagsReplacedSheet2 = queryDateTagsReplacer.ReplaceDateTagsWithDateValues(
                     lbQueries_REP.SelectedValue.ToString(), calendarOpenedDateTaken,
                     queryWithoutDateTagsReplacedSheet2, datePickerStart_REP, datePickerEnd_REP);

                tbQueriesBodySheet2_REP.Text = queryWithDateTagsReplacedSheet2;
            }

            calendarOpenedDateTaken = false;
        }



        private void Sheet1ButtonSettings()
        {
            tbQueriesBodySheet1_REP.Visibility = Visibility.Visible;
            tbQueriesBodySheet2_REP.Visibility = Visibility.Hidden;

            sheet1Button_REP.BorderBrush = mainThemeColor;
            sheet2Button_REP.BorderBrush = Brushes.Transparent;
        }

        private void Sheet2ButtonSettings()
        {
            tbQueriesBodySheet1_REP.Visibility = Visibility.Hidden;
            tbQueriesBodySheet2_REP.Visibility = Visibility.Visible;

            sheet1Button_REP.BorderBrush = Brushes.Transparent;
            sheet2Button_REP.BorderBrush = mainThemeColor;
        }


        private void AllReportsUncheckedkActions()
        {
            // Class Instances
            Directories directories = new Directories();

            buttonExecuteAll_REP.Visibility = Visibility.Hidden;
            buttonCreate_REP.Visibility = Visibility.Visible;

            lbQueries_REP.ItemsSource = null; // Clear itemsSource before deleting everything, otherwise we get runtime error
            lbQueries_REP.Items.Clear(); // Clear items and refresh/reload the whole list

            directories.LoadReportsIntoListboxFromJSONUniqueNames(lbQueries_REP);
        }

        private void ScheduledForTodayCheckedActions()
        {
            // Class Instance 
            ScheduledForToday todayOnly = new ScheduledForToday();

            var converter = new BrushConverter();
            var mainThemeColor = (Brush)converter.ConvertFromString("#FF2196F3");

            tbQueriesBodySheet1_REP.Text = "";
            tbQueriesBodySheet2_REP.Text = "";

            buttonExecuteAll_REP.Visibility = Visibility.Visible;
            buttonCreate_REP.Visibility = Visibility.Hidden;

            // Clear items and refresh/reload the whole list
            lbQueries_REP.ItemsSource = null;
            lbQueries_REP.Items.Clear();

            var list = todayOnly.CalculateReportsRequiredForToday(lbQueries_REP);

            lbQueries_REP.Items.Clear(); // Clear before appylying new items
            lbQueries_REP.ItemsSource = list;

            sheet1Button_REP.Visibility = Visibility.Hidden;
            sheet2Button_REP.Visibility = Visibility.Hidden;

            tBlockSheets.Visibility = Visibility.Hidden;
            tBlockConnection.Visibility = Visibility.Hidden;
            tbReportConnection_REP.Visibility = Visibility.Hidden;

            datePickerStart_REP.Visibility = Visibility.Hidden;
            datePickerEnd_REP.Visibility = Visibility.Hidden;
        }
    }
}