﻿using BespokeFusion;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace EVS_Support_Tool.Reporting_Tool
{
    /// <summary>
    /// Interaction logic for UserControl_AddModify.xaml
    /// </summary>
    public partial class UserControl_AddModify : UserControl
    {

        private string _loggerText;

        public UserControl_AddModify()
        {
            InitializeComponent();

            StartupSettings();
        }

        private void SetupDeleteButton_AM_Click(object sender, RoutedEventArgs e)
        {
            DeleteButtonAction();
        }

        private void ButtonNewReport_AM_Click(object sender, RoutedEventArgs e)
        {
            NewButtonAction();
        }

        private void SetupCreateButton_AM_Click(object sender, RoutedEventArgs e)
        {
            CreateNewRecordInExistingJSONFile();
        }

        private void ListboxQueries_AM_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListboxSelectionChangedActions();
        }

        private void SetupUpdateButton_AM(object sender, RoutedEventArgs e)
        {
            UpdateButtonActions();
        }

        private void SetupSheet2AddButton_AM_Click(object sender, RoutedEventArgs e)
        {
            SupportingMethods supportingMethods = new SupportingMethods();
            supportingMethods.AddSheet2(setupSheet2SQL_AM, setupSheet2Name_AM, setupSheet2AddButton_AM, setupSheet2DelButton_AM);
        }

        private void SetupSheet2DelButton_AM_Click(object sender, RoutedEventArgs e)
        {
            SupportingMethods supportingMethods = new SupportingMethods();
            supportingMethods.DeteleSheet2(setupSheet2SQL_AM, setupSheet2Name_AM, setupSheet2AddButton_AM, setupSheet2DelButton_AM);
        }

        private void SetupCancelButton_AM_Click(object sender, RoutedEventArgs e)
        {
            ResetToDefaultSetup();
            ResetToDefaultSetup();
        }

        private void SetupType_AM_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DaysOnStringVisibility();
        }

        private void SetupSendOnDay_AM_GotFocus(object sender, RoutedEventArgs e)
        {
            setupSendOnDay_AM.Width = 157;
            setupSendOnDay_AM.Height = 112;

            setupSendOnCloseButton_AM.Visibility = Visibility.Visible;
        }
        private void SetupSendOnCloseButton_AM_Click(object sender, RoutedEventArgs e)
        {
            JSONConfigTagsOperations ConfigTags = new JSONConfigTagsOperations();

            string str = ConfigTags.CreateSendOnDaysString(setupSendOnDay_AM, setupSendOnDayString_AM.Text);
            setupSendOnDayString_AM.Text = str;

            // lb.SelectedIndex = -1;

            setupSendOnDay_AM.Width = 157;
            setupSendOnDay_AM.Height = 28;

            setupSendOnCloseButton_AM.Visibility = Visibility.Collapsed;
        }


        private void SetupSQLSheet1_AM_GotFocus(object sender, RoutedEventArgs e)
        {
            setupSheet1SQL_AM.Width = 520;
            setupSheet1SQL_AM.Height = 255;

            setupSQL1CloseButton_AM.Visibility = Visibility.Visible;
        }
        private void SetupSQL1CloseButton_AM_Click(object sender, RoutedEventArgs e)
        {
            setupSheet1SQL_AM.Width = 365;
            setupSheet1SQL_AM.Height = 25;

            setupSQL1CloseButton_AM.Visibility = Visibility.Collapsed;
        }


        private void SetupSQLSheet2_AM_GotFocus(object sender, RoutedEventArgs e)
        {
            setupSheet2SQL_AM.Width = 520;
            setupSheet2SQL_AM.Height = 255;

            setupSQL2CloseButton_AM.Visibility = Visibility.Visible;
        }
        private void SetupSQL2CloseButton_AM_Click(object sender, RoutedEventArgs e)
        {
            setupSheet2SQL_AM.Width = 365;
            setupSheet2SQL_AM.Height = 25;

            setupSQL2CloseButton_AM.Visibility = Visibility.Collapsed;
        }


        private void SetupEmailBody_AM_GotFocus(object sender, RoutedEventArgs e)
        {
            setupEmailBody_AM.Width = 520;
            setupEmailBody_AM.Height = 145;

            setupEmailCloseButton_AM.Visibility = Visibility.Visible;
        }
        private void SetupEmailCloseButton_AM_Click(object sender, RoutedEventArgs e)
        {
            setupEmailBody_AM.Width = 365;
            setupEmailBody_AM.Height = 25;

            setupEmailCloseButton_AM.Visibility = Visibility.Collapsed;
        }


        private void SetupSendTo_AM_GotFocus(object sender, RoutedEventArgs e)
        {
            setupSendTo_AM.Width = 520;
            setupSendTo_AM.Height = 90;

            setupSendToCloseButton_AM.Visibility = Visibility.Visible;
        }
        private void SetupSendToCloseButton_AM_Click(object sender, RoutedEventArgs e)
        {
            setupSendTo_AM.Width = 365;
            setupSendTo_AM.Height = 25;

            setupSendToCloseButton_AM.Visibility = Visibility.Collapsed;
        }


        private void StartupSettings()
        {
            // Class Instances
            LoggerMessage logger = new LoggerMessage();
            Directories directories = new Directories();

            setupSQL1CloseButton_AM.Visibility = Visibility.Collapsed;
            setupSQL2CloseButton_AM.Visibility = Visibility.Collapsed;
            setupSendToCloseButton_AM.Visibility = Visibility.Collapsed;
            setupEmailCloseButton_AM.Visibility = Visibility.Collapsed;
            setupSendOnCloseButton_AM.Visibility = Visibility.Collapsed;

            setupSheet2DelButton_AM.IsEnabled = false;
            setupSheet2Name_AM.IsEnabled = false;
            setupSheet2SQL_AM.IsEnabled = false;
            setupSheet2Name_AM.IsEnabled = false;

            setupDeleteButton_AM.Visibility = Visibility.Collapsed;
            setupUpdateButton_AM.Visibility = Visibility.Collapsed;
            setupCreateButton_AM.Visibility = Visibility.Collapsed;

            setupGrid.Visibility = Visibility.Collapsed;

            directories.LoadReportsIntoListboxFromJSONUniqueNames(listboxQueries_AM);

            _loggerText = logger.MessageQueriesLoaded();
            tbLogger_AM.Text = _loggerText;
        }


        private void DeleteButtonAction()
        {
            // Class Instances
            PopupMessage popup = new PopupMessage();

            var clickOKCancel = new CustomMaterialMessageBox();
            clickOKCancel = popup.MessageBoxConfirmationPopup("Are you sure you want to delete this report permanently?", clickOKCancel);

            if (clickOKCancel.Result == MessageBoxResult.OK)
            {
                // Class Instances
                Directories directories = new Directories();
                JSONConfigTagsOperations configTagsOps = new JSONConfigTagsOperations();
                LoggerMessage logger = new LoggerMessage();

                configTagsOps.DeleteExistingJSONElement(listboxQueries_AM.SelectedValue.ToString());

                // Load message into logger box
                _loggerText = logger.MessageReportDeleted();
                tbLogger_AM.Text += _loggerText;

                // Clear items and refresh/reload the whole list
                listboxQueries_AM.Items.Clear();
                directories.LoadReportsIntoListboxFromJSONUniqueNames(listboxQueries_AM);

                ResetToDefaultSetup();
                ResetToDefaultSetup();
            }
        }


        private void NewButtonAction()
        {
            ResetToDefaultSetup();
            ResetToDefaultSetup();

            setupGrid.Visibility = Visibility.Visible;
            setupCreateButton_AM.Visibility = Visibility.Visible;

            setupDeleteButton_AM.Visibility = Visibility.Collapsed;
            setupUpdateButton_AM.Visibility = Visibility.Collapsed;
        }


        private void CreateNewRecordInExistingJSONFile()
        {
            if (setupUniqName_AM.Text != "" && setupType_AM.SelectedValue != null)
            {
                if (setupType_AM.Text == "daily" && setupSendOnDayString_AM.Text == "")
                {
                    // Class Instance 
                    PopupMessage popup = new PopupMessage();

                    popup.MessageBoxErrorPopup("Send On Days string can't be empty when using Daily type of the report.");
                }

                else
                {
                    // Class Instances
                    LoggerMessage logger = new LoggerMessage();
                    JSONConfigTagsOperations operations = new JSONConfigTagsOperations();

                    bool reportAlreadyExists;
                    List<JSONConfigTags> jsonItems;

                    reportAlreadyExists = false;
                    jsonItems = operations.JSONLoadFile();


                    for (int i = 0; i < jsonItems.Count; i++)
                    {
                        // Loop through JSON list and compare with newly entered new report unique name
                        if (jsonItems[i].UniqueName == setupUniqName_AM.Text)
                        {
                            reportAlreadyExists = true;
                            break;
                        }
                    }
                    

                    if (!reportAlreadyExists)
                    {
                        // Class Instances
                        JSONConfigTagsOperations ConfigTags = new JSONConfigTagsOperations();
                        Directories directories = new Directories();
                        SupportingMethods supportingMethods = new SupportingMethods();


                        bool sheet2Required;
                        sheet2Required = supportingMethods.Sheet2IsRequiredBeforeSetupHasBeenDone(setupSheet2AddButton_AM, setupSheet2DelButton_AM);


                        // Create 2 sheet if required
                        if (sheet2Required == true && setupSheet2SQL_AM.Text == "" && setupSheet2Name_AM.Text == "")
                        {
                            // Class Instance 
                            PopupMessage popup = new PopupMessage();

                            // Load message into logger box
                            _loggerText = logger.MessageJSONRecordNotCreated();
                            tbLogger_AM.Text += _loggerText;

                            popup.MessageBoxErrorPopup("Second sheet SQL query and unique name fields can't be empty.");

                            return; // prevent executing further method code if something is wrong with sheet 2
                        }


                        // Create JSON tags
                        ConfigTags.JSONWriteNewConfigTag(
                            setupUniqName_AM, setupType_AM, setupConnection_AM, setupSheet1SQL_AM, setupSheet1Name_AM,
                            setupSheet2SQL_AM, setupSheet2Name_AM, setupSendTo_AM, setupEmailBody_AM, setupSendOnDay_AM, setupSendOnDayString_AM.Text);

                        // Load appropriate message into logger box
                        if (sheet2Required)
                        {
                            _loggerText = logger.MessageNewReportFileCreatedWithTwoSheets();
                            tbLogger_AM.Text += _loggerText;
                        }

                        else
                        {
                            _loggerText = logger.MessageNewReportFileCreatedWithOneSheet();
                            tbLogger_AM.Text += _loggerText;
                        }


                        // Clear items and refresh/reload the whole list
                        listboxQueries_AM.Items.Clear();
                        directories.LoadReportsIntoListboxFromJSONUniqueNames(listboxQueries_AM);

                        ResetToDefaultSetup();
                        // }
                    }
                    else
                    {
                        // Class Instance 
                        PopupMessage popup = new PopupMessage();

                        popup.MessageBoxErrorPopup("File already exists! Please try another unique name.");

                        // Load message into logger box
                        _loggerText = logger.MessageNewReportFileCreatedFailedAlreadyExists();
                        tbLogger_AM.Text += _loggerText;
                    }
                }
            }

            else
            {
                // Class Instance 
                PopupMessage popup = new PopupMessage();

                popup.MessageBoxErrorPopup("Unique Report Name and Type fields can't be empty.");
            }
        }


        private void DaysOnStringVisibility()
        {
            // Activate DaySendOnString only when daily type has been chosen
            if (setupType_AM.SelectedIndex == -1)
            {
                setupSendOnDay_AM.IsEnabled = false;
            }

            else if ((setupType_AM.SelectedItem as ComboBoxItem).Content.ToString() == "daily")
            {
                setupSendOnDay_AM.IsEnabled = true;
            }

            else
            {
                setupSendOnDay_AM.IsEnabled = false;
            }
        }


        private void ListboxSelectionChangedActions()
        {
            // Class Instances
            ConfigTagUniqueName uniqName = new ConfigTagUniqueName();
            ConfigTagType type = new ConfigTagType();
            ConfigTagConnection conn = new ConfigTagConnection();
            ConfigTagSheet1SQL sheet1Sql = new ConfigTagSheet1SQL();
            ConfigTagSheet1DisplayName sheet1Name = new ConfigTagSheet1DisplayName();
            ConfigTagSheet2SQL sheet2Sql = new ConfigTagSheet2SQL();
            ConfigTagSheet2DisplayName sheet2Name = new ConfigTagSheet2DisplayName();
            ConfigTagOutlookSendTo sendTo = new ConfigTagOutlookSendTo();
            ConfigTagOutlookEmailBody body = new ConfigTagOutlookEmailBody();
            ConfigTagSendOnDay sendOn = new ConfigTagSendOnDay();
            SupportingMethods supportingMethods = new SupportingMethods();

            setupDeleteButton_AM.Visibility = Visibility.Visible;
            setupUpdateButton_AM.Visibility = Visibility.Visible;
            setupCreateButton_AM.Visibility = Visibility.Collapsed;
            setupGrid.Visibility = Visibility.Visible;



            if (listboxQueries_AM.SelectedValue != null) // Skip any actions if we haven't selected any report
            {
                uniqName.ShowConfigTagContentToTextboxOrCombobox(setupUniqName_AM, listboxQueries_AM.SelectedValue.ToString());
                type.ShowConfigTagContentToTextboxOrCombobox(setupType_AM, listboxQueries_AM.SelectedValue.ToString());
                conn.ShowConfigTagContentToTextboxOrCombobox(setupConnection_AM, listboxQueries_AM.SelectedValue.ToString());
                sheet1Sql.ShowConfigTagContentToTextboxOrCombobox(setupSheet1SQL_AM, listboxQueries_AM.SelectedValue.ToString());
                sheet1Name.ShowConfigTagContentToTextboxOrCombobox(setupSheet1Name_AM, listboxQueries_AM.SelectedValue.ToString());
                sheet2Sql.ShowConfigTagContentToTextboxOrCombobox(setupSheet2SQL_AM, listboxQueries_AM.SelectedValue.ToString());
                sheet2Name.ShowConfigTagContentToTextboxOrCombobox(setupSheet2Name_AM, listboxQueries_AM.SelectedValue.ToString());
                sendTo.ShowConfigTagContentToTextboxOrCombobox(setupSendTo_AM, listboxQueries_AM.SelectedValue.ToString());
                body.ShowConfigTagContentToTextboxOrCombobox(setupEmailBody_AM, listboxQueries_AM.SelectedValue.ToString());
                sendOn.ShowConfigTagContentToTextboxOrCombobox(setupSendOnDay_AM, setupSendOnDayString_AM, listboxQueries_AM.SelectedValue.ToString());
                //  sendOn.ShowConfigTagContentToTextboxOrCombobox()
            }

            if (setupSheet2SQL_AM.Text != "")
            {
                supportingMethods.AddSheet2(setupSheet2SQL_AM, setupSheet2Name_AM, setupSheet2AddButton_AM, setupSheet2DelButton_AM);
            }

            else
            {
                supportingMethods.DeteleSheet2(setupSheet2SQL_AM, setupSheet2Name_AM, setupSheet2AddButton_AM, setupSheet2DelButton_AM);
            }
        }


        private void UpdateButtonActions()
        {
            // Class Instances
            PopupMessage msg = new PopupMessage();
            Directories directories = new Directories();

            var clickOKCancel = new CustomMaterialMessageBox();
            clickOKCancel = msg.MessageBoxConfirmationPopup("Are you sure you want to proceed?" +
                "\n\nNote that this action will overwrite the current setup of this report.", clickOKCancel);

            if (clickOKCancel.Result == MessageBoxResult.OK)
            {
                // Class Instances
                JSONConfigTagsOperations configTags = new JSONConfigTagsOperations();
                LoggerMessage logger = new LoggerMessage();

                configTags.DeleteExistingJSONElement(listboxQueries_AM.SelectedValue.ToString());

                configTags.JSONWriteNewConfigTag(setupUniqName_AM, setupType_AM, setupConnection_AM,
                    setupSheet1SQL_AM, setupSheet1Name_AM, setupSheet2SQL_AM, setupSheet2Name_AM,
                    setupSendTo_AM, setupEmailBody_AM, setupSendOnDay_AM, setupSendOnDayString_AM.Text);
                

                // Load message into logger box
                _loggerText = logger.MessageReportUpdated();
                tbLogger_AM.Text += _loggerText;

                // Clear items and refresh/reload the whole list
                listboxQueries_AM.Items.Clear();
                directories.LoadReportsIntoListboxFromJSONUniqueNames(listboxQueries_AM);

                ResetToDefaultSetup();
                ResetToDefaultSetup();
            }
        }


        private void ResetToDefaultSetup()
        {
            setupUniqName_AM.Text = "";
            setupType_AM.SelectedIndex = -1;
            setupConnection_AM.SelectedIndex = 0;
            setupSheet1SQL_AM.Text = "";
            setupSheet1Name_AM.Text = "Report";
            setupSheet2SQL_AM.Text = "";
            setupSheet2Name_AM.Text = "";
            setupSendTo_AM.Text = "";
            setupEmailBody_AM.Text = "Please find report attached.";

            setupSheet2AddButton_AM.IsEnabled = true;
            setupSheet2DelButton_AM.IsEnabled = false;

            setupSheet2Name_AM.IsEnabled = false;
            setupSheet2SQL_AM.IsEnabled = false;

            setupGrid.Visibility = Visibility.Collapsed;

            setupSendOnDay_AM.IsEnabled = false;
            setupSendOnDay_AM.SelectedIndex = -1;
            setupSendOnDayString_AM.Text = "";

            setupDeleteButton_AM.Visibility = Visibility.Collapsed;
            setupUpdateButton_AM.Visibility = Visibility.Collapsed;

            listboxQueries_AM.UnselectAll();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            setupSendOnDay_AM.SelectedIndex = -1;
        }

        private void TextboxLogger_AM_TextChanged(object sender, TextChangedEventArgs e)
        {
            // Always show the last line in textbox logger when text is being added
            if (tbLogger_AM.LineCount != -1)
            {
                tbLogger_AM.ScrollToLine(tbLogger_AM.LineCount - 1);
            }
        }
    }
}