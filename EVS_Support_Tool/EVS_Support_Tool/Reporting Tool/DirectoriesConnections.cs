﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace EVS_Support_Tool.Reporting_Tool
{
    /// <summary>
    /// Delete all files in _OUTPUT FILES directory everytime form is loaded
    /// </summary>

    class Directories
    {
        public void DeleteFilesInOutputFilesDirectory()
        {

            var dir = GetDirectoryPathForOutputFilesFolder();

            try
            {     
                DirectoryInfo di = new DirectoryInfo(dir);
                di.Delete(true);
            }

            catch (Exception DeleteFilesException)
            {
                // Class Instance 
                PopupMessage popup = new PopupMessage();

                popup.MessageBoxErrorPopup("An error occured while clearing files in OUTPUT directory...\n\n" + DeleteFilesException.Message);
            }
        }


        public string GetDirectoryPathForOutputFilesFolder()
        {
            string outputFilesPath = null;

            try
            {
                Directory.CreateDirectory(@"C:\Temp_reportingTool\");

                outputFilesPath = @"C:\Temp_reportingTool\";
            }

            catch (Exception OutputFolderPathException)
            {
                // Class Instance 
                PopupMessage popup = new PopupMessage();

                using (new StackedCursorOverride(Cursors.Arrow)) // Cursors change
                {
                    popup.MessageBoxErrorPopup("Unable to access the _OUTPUT FILES directory...\n\n" + OutputFolderPathException.Message);
                }
            }

            return outputFilesPath;
        }


        public string GetDirectoryPathAndFilenameForJSONReportConfigs()
        {
            string dirFile = "";

            try
            {
                dirFile = @"G:\Shared - HireRight-\Development\HTS\EVS_Support_Tool\_JSON REPORT CONFIGS\JSON Configs.json";
            }

            catch (Exception JSONFolderPathException)
            {
                // Class Instance 
                PopupMessage popup = new PopupMessage();

                using (new StackedCursorOverride(Cursors.Arrow)) // Cursors change
                {
                    popup.MessageBoxErrorPopup("An error occurred while accesssing the _JSON REPORT CONFIGS directory...\n\n" + JSONFolderPathException.Message);
                }
            }

            return dirFile;
        }


        public void LoadReportsIntoListboxFromJSONUniqueNames(ListBox lbQueries)
        {
            // Class Instances
            JSONConfigTagsOperations operations = new JSONConfigTagsOperations();

            string displayName;
            List<JSONConfigTags> jsonItems;

            jsonItems = operations.JSONLoadFile();

            // Sort items by type + unique name + connection before appearing in the textbox
            var newList = jsonItems.OrderBy(x => x.UniqueName).OrderBy(x => x.Type).ToList();

            for (int i = 0; i < newList.Count; i++)
            {
                displayName = $"{newList[i].Type}\t\t  {newList[i].UniqueName}";

                lbQueries.Items.Add(displayName);
            }
        }
    }


    class SQLDBConnections
    {
        private readonly string _connectionString_EVS45 = "Data Source=10.88.29.17;Initial Catalog=EVS45;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private readonly string _connectionString_EVS_DW = "Data Source=10.88.29.18;Initial Catalog=EVS_DW;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private readonly string _connectionString_OLAF_RBSAgencyCheck = "Data Source=10.88.29.19;Initial Catalog=RBSAgencyCheck;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private readonly string _connectionString_KBWStats = "Data Source=10.88.29.18;Initial Catalog=KBWStats;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private readonly string _connectionString_Billing = "Data Source=10.88.29.18;Initial Catalog=Billing;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public string GetDBConnectionStringForJSONConfigFile(string value)
        {
            switch (value)
            {
                case "EVS45":
                    value = _connectionString_EVS45;
                    break;

                case "EVS_DW":
                    value = _connectionString_EVS_DW;
                    break;

                case "OLAF_RBSAgencyCheck":
                    value = _connectionString_OLAF_RBSAgencyCheck;
                    break;

                case "KBWSTATS":
                    value = _connectionString_KBWStats;
                    break;

                case "BILLING":
                    value = _connectionString_Billing;
                    break;
            }

            return value;
        }
    }
}