﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;

namespace EVS_Support_Tool.Reporting_Tool
{
    //
    // JSON READER CLASSES START
    //
    abstract class JSONConfigTagsReader
    {
        public virtual string ReadTagContent(string report)
        {
            // Class Instances 
            JSONConfigTagsOperations operations = new JSONConfigTagsOperations();

            string classType;
            string returnTagString = "";

            System.Console.WriteLine();

            List<JSONConfigTags> jsonItems;

            jsonItems = operations.JSONLoadFile();

            for (int i = 0; i < jsonItems.Count; i++)
            {
                report = report.Substring(report.LastIndexOf('\t') + 1);  // Cut the type at the beginning
                report = report.Trim();

                classType = GetType().Name; // Get derived class name and chose action according to its name

                if (jsonItems[i].UniqueName == report)
                {
                    switch (classType)
                    {
                        case "ConfigTagUniqueName":

                            returnTagString = jsonItems[i].UniqueName;
                            break;

                        case "ConfigTagType":

                            returnTagString = jsonItems[i].Type;
                            break;

                        case "ConfigTagConnection":

                            returnTagString = jsonItems[i].Connection;
                            break;

                        case "ConfigTagSheet1SQL":

                            returnTagString = jsonItems[i].Sheet1SQL;
                            break;

                        case "ConfigTagSheet1DisplayName":

                            returnTagString = jsonItems[i].Sheet1DisplayName;
                            break;

                        case "ConfigTagSheet2SQL":

                            returnTagString = jsonItems[i].Sheet2SQL;
                            break;

                        case "ConfigTagSheet2DisplayName":

                            returnTagString = jsonItems[i].Sheet2DisplayName;
                            break;

                        case "ConfigTagOutlookSendTo":

                            returnTagString = jsonItems[i].OutlookSendTo;
                            break;

                        case "ConfigTagOutlookEmailBody":

                            returnTagString = jsonItems[i].OutlookEmailBody;
                            break;

                        case "ConfigTagSendOnDay":

                            returnTagString = jsonItems[i].SendOnDay;
                            break;
                    }
                }
            }

            return returnTagString;
        }

        

        // Methods to override with TextBox and ComboBox
        public virtual string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return tb.Text = ReadTagContent(rep);
        }

        public virtual string ShowConfigTagContentToTextboxOrCombobox(ComboBox cb, string rep)
        {
            return cb.Text = ReadTagContent(rep);
        }

        public virtual string ShowConfigTagContentToTextboxOrCombobox(ListBox lb, TextBox tbSendOnString, string rep)
        {
            string str = ReadTagContent(rep);
            tbSendOnString.Text = str;

            return str;
        }

    }


    class ConfigTagUniqueName : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }


    class ConfigTagType : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(ComboBox cb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(cb, rep);
        }
    }


    class ConfigTagConnection : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(ComboBox cb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(cb, rep);
        }
    }


    class ConfigTagSheet1SQL : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }


    class ConfigTagSheet1DisplayName : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }

    class ConfigTagSheet2SQL : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }


    class ConfigTagSheet2DisplayName : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }


    class ConfigTagOutlookSendTo : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }


    class ConfigTagOutlookEmailBody : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(TextBox tb, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(tb, rep);
        }
    }


    class ConfigTagSendOnDay : JSONConfigTagsReader
    {
        public override string ReadTagContent(string rep)
        {
            return base.ReadTagContent(rep);
        }

        public override string ShowConfigTagContentToTextboxOrCombobox(ListBox lb, TextBox tbSendOnString, string rep)
        {
            return base.ShowConfigTagContentToTextboxOrCombobox(lb, tbSendOnString, rep);
        }
    }






    //
    // JSON READER CLASSES END
    //





    //
    // JSON OPERATIONS START
    //

    class JSONConfigTags
    {
        public string UniqueName { get; set; }
        public string Type { get; set; }
        public string Connection { get; set; }
        public string Sheet1SQL { get; set; }
        public string Sheet1DisplayName { get; set; }
        public string Sheet2SQL { get; set; }
        public string Sheet2DisplayName { get; set; }
        public string OutlookSendTo { get; set; }
        public string OutlookEmailBody { get; set; }
        public string SendOnDay { get; set; }

        // Ctor without parameters, mandatory for JSON
        public JSONConfigTags()
        {
        }
    }


    class JSONConfigTagsOperations
    {
        private List<JSONConfigTags> _jsonItems = new List<JSONConfigTags>();


        public string CreateSendOnDaysString(ListBox lbSendOn, string sendOnString)
        {
            string days = "";

            foreach (var item in lbSendOn.SelectedItems)
            {
                string temp = item.ToString();

                // System.Windows.Controls.ListBoxItem: Tuesday, removing the fist part
                days += temp.Substring(37, temp.Length - 37) + ",";
            }

            // Save current sendOnString before deletion and creation of a new one
            if(lbSendOn.SelectedItems.Count == 0)
            {
                days = sendOnString;
            }

            return days;
        }


        public void JSONWriteNewConfigTag(TextBox uniqN, ComboBox type, ComboBox con, TextBox sql1,
            TextBox sheet1, TextBox sql2, TextBox sheet2, TextBox sendTo, TextBox body, ListBox sendOn, string sendOnString)
        {
            _jsonItems = JSONLoadFile();

            JSONConfigTags tag = new JSONConfigTags
            {
                UniqueName = uniqN.Text,
                Type = type.Text,
                Connection = con.Text,
                Sheet1SQL = sql1.Text,
                Sheet1DisplayName = sheet1.Text,
                Sheet2SQL = sql2.Text,
                Sheet2DisplayName = sheet2.Text,
                OutlookSendTo = sendTo.Text,
                OutlookEmailBody = body.Text,
                SendOnDay = CreateSendOnDaysString(sendOn, sendOnString)

            };

            _jsonItems.Add(tag);

            JSONSaveFile(_jsonItems);
        }



        // Saver
        public void JSONSaveFile(List<JSONConfigTags> list)
        {
            Directories directories = new Directories();

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);
            File.WriteAllText(directories.GetDirectoryPathAndFilenameForJSONReportConfigs(), json);
        }

        // Loader
        public List<JSONConfigTags> JSONLoadFile()
        {
            // Class Instances
            Directories directories = new Directories();

            string jsonString;
            List<JSONConfigTags> items;

            using (StreamReader sr = new StreamReader(directories.GetDirectoryPathAndFilenameForJSONReportConfigs()))
            {
                jsonString = sr.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<JSONConfigTags>>(jsonString);
            }

            return items;
        }




        public void DeleteExistingJSONElement(string report)
        {
            // Class Instance 
            ConfigTagUniqueName configTagUniqueName = new ConfigTagUniqueName();

            string JASONAttributeToDelete = configTagUniqueName.ReadTagContent(report);

            // Loading JSON
            _jsonItems = JSONLoadFile();

            var x = _jsonItems.Single(r => r.UniqueName == JASONAttributeToDelete);
            _jsonItems.Remove(x);

            // Saving JSON
            JSONSaveFile(_jsonItems);
        }
    }

    //
    // JSON OPERATIONS END
    //
}
