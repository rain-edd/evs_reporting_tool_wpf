﻿using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using Excel = Microsoft.Office.Interop.Excel;

namespace EVS_Support_Tool.Reporting_Tool
{
    class ReportCreator
    {
        // Static properties
        public static string ReportSubject_static { get; private set; }
        public static FileInfo ReportPathFilenameExtension_static { get; private set; }
        public static int ReportTotalRowsSheet1_static { get; set; }
        public static int ReportTotalRowsSheet2_static { get; set; }

        private ExcelWorksheet _worksheet;

        public void ExecuteFileCreationMethods(string report, DatePicker datePickStart, DatePicker datePickEnd, TextBox tbSQL1, TextBox tbSQL2)
        {
            ReportSubject_static = SetStringOfReportSubject(report, datePickStart, datePickEnd);
            ReportPathFilenameExtension_static = SetStringOfReportPathWithFilenameWithExtension();

            CreateExcelFileFromTableDataReceivedData(report, tbSQL1, tbSQL2);
        }


        private string SetStringOfReportSubject(string report, DatePicker datePickStart, DatePicker datePickEnd)
        {
            // Class Instances
            DatesCalculations dates = new DatesCalculations();
            ConfigTagUniqueName configTagUniqueName = new ConfigTagUniqueName();

            string subject = "";
            string startDate = "";
            string endDate = "";
            string reportName;

            reportName = configTagUniqueName.ReadTagContent(report);

            // Assign start-end formatted values if date presents in a DatePicker boxes
            if (datePickStart.SelectedDate.ToString() != "")
            {
                startDate = datePickStart.SelectedDate.Value.ToString("d MMM");
            }

            if (datePickEnd.SelectedDate.ToString() != "")
            {
                endDate = datePickEnd.SelectedDate.Value.ToString("d MMM");
            }

            // Creating subject accordingly
            if ((startDate == "" && endDate == "") || endDate == "")
            {
                subject = $"{reportName} ({dates.GetTodayDay().ToString("d MMM")})";
            }

            else if (startDate == endDate)
            {
                subject = $"{reportName} ({endDate})";
            }
            else if (startDate != endDate)
                subject = $"{reportName} ({startDate} - {endDate})";

            return subject;
        }


        private FileInfo SetStringOfReportPathWithFilenameWithExtension()
        {
            // Class instances
            Directories directories = new Directories();

            var outputFilesPath = directories.GetDirectoryPathForOutputFilesFolder();
            var subject = ReportSubject_static;

            FileInfo output;

            output = new FileInfo($"{outputFilesPath}{subject}.xlsx");

            return output;
        }

        private void SetTotalRowsInReportToStaticVariable(DataTable dt, string currentlyUsedForSheet)
        {
            // For sheet 1
            if (currentlyUsedForSheet == "Using Sheet 1")
            {
                if (dt == null)
                {
                    ReportTotalRowsSheet1_static = -1;

                }

                else if (dt.Rows.Count > 0)
                {
                    ReportTotalRowsSheet1_static = dt.Rows.Count;
                }

                else
                {
                    ReportTotalRowsSheet1_static = 0;
                }
            }

            // For sheet 2
            if (currentlyUsedForSheet == "Using Sheet 2")
            {
                if (dt == null)
                {
                    ReportTotalRowsSheet2_static = -1;

                }

                else if (dt.Rows.Count > 0)
                {
                    ReportTotalRowsSheet2_static = dt.Rows.Count;
                }

                else
                {
                    ReportTotalRowsSheet2_static = 0;
                }
            }
        }


        private DataTable FillDataTableWithDataReceivedFromSQLQuery(string report, TextBox tb, string currentSheetInUsage)
        {
            // Class Instances
            SQLDBConnections conn = new SQLDBConnections();
            ConfigTagConnection connTag = new ConfigTagConnection();

            // Get tag from JSON and parse tag to the connection string itself
            string connectionString = conn.GetDBConnectionStringForJSONConfigFile(connTag.ReadTagContent(report));

            string query = tb.Text; // Read the updated with date tags query from textbox

            using (SqlConnection sqlConn = new SqlConnection(connectionString))

            using (SqlCommand cmd = new SqlCommand(query, sqlConn))
            {
                DataTable dt = null;
                cmd.CommandTimeout = 650; // Database connection timeout period limit

                try
                {
                    sqlConn.Open();
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader(), LoadOption.OverwriteChanges);

                    SetTotalRowsInReportToStaticVariable(dt, currentSheetInUsage);
                }

                catch (Exception NoConnectionException)
                {
                    // Class Instance 
                    PopupMessage popup = new PopupMessage();

                    SetTotalRowsInReportToStaticVariable(dt, currentSheetInUsage);

                    using (new StackedCursorOverride(Cursors.Arrow)) // Cursors change
                    {
                        popup.MessageBoxErrorPopup("Can't connect. Please check the connection...\n\n" + NoConnectionException.Message);
                    }
                }

                return dt;
            }
        }

        

        private TableStyles GetExcelFormatAsTableStyleAccordingToReportType(string report)
        {
            // Class Instance
            ConfigTagType configTagType = new ConfigTagType();

            string reportType = configTagType.ReadTagContent(report);

            TableStyles style = TableStyles.Custom;

            // Applying FormatAsTable Style according to report type
            switch (reportType)
            {
                case "daily":
                    style = TableStyles.Medium21;

                    break;

                case "weekly":
                    style = TableStyles.Medium20;

                    break;

                case "monthly":
                    style = TableStyles.Medium17;

                    break;
            }

            return style;
        }



        private void CreateExcelFileFromTableDataReceivedData(string report, TextBox tbSQL1, TextBox tbSQL2)
        {
            // Class Instances
            ConfigTagSheet1DisplayName configTagSheet1DisplayName = new ConfigTagSheet1DisplayName();
            SupportingMethods supportingMethods = new SupportingMethods();

            bool sheet2Required;
            string sheetName;

            List<int> listOfColumnIndexesWithDatetimeValuesSheet1;
            List<int> listOfColumnIndexesWithDatetimeValuesSheet2;

            FileInfo fileInfo;
            DataTable datatable;

            sheet2Required = supportingMethods.Sheet2IsRequiredAfterSetupHasBeenDone(report);

            fileInfo = ReportPathFilenameExtension_static;


            // Sheet 1 actions
            datatable = FillDataTableWithDataReceivedFromSQLQuery(report, tbSQL1, "Using Sheet 1");

            if (datatable == null) // Stop method in case we have an empty datatable
            {
                return;
            }

            listOfColumnIndexesWithDatetimeValuesSheet1 = WriteDatetimeColumnsIndexesFromSheetToList(datatable);

            using (ExcelPackage pck = new ExcelPackage(fileInfo))
            {
                // We need to remove any sheets before creating new ones, because in case of duplicates we will get an error
                foreach (var sheet in pck.Workbook.Worksheets.ToList())
                {
                    if (pck.Workbook.Worksheets.Count > 0)
                    {
                        pck.Workbook.Worksheets.Delete(sheet);
                    }
                }

                sheetName = configTagSheet1DisplayName.ReadTagContent(report);

                _worksheet = pck.Workbook.Worksheets.Add(sheetName);

                _worksheet.Cells["A1"].LoadFromDataTable(datatable, true, GetExcelFormatAsTableStyleAccordingToReportType(report));
                pck.Save();
            }
            
            ApplyNumberFormattingForDatetimeColumnsAtExcelSheet(listOfColumnIndexesWithDatetimeValuesSheet1, 1);  // For Sheet 1


            // Sheet 2 actions
            if (sheet2Required == true)
            {
                ConfigTagSheet2DisplayName configTagSheet2DisplayName = new ConfigTagSheet2DisplayName();
                sheetName = configTagSheet2DisplayName.ReadTagContent(report);

                datatable = FillDataTableWithDataReceivedFromSQLQuery(report, tbSQL2, "Using Sheet 2");

                if (datatable == null) // Stop method in case we have an empty datatable
                {
                    return;
                }

                listOfColumnIndexesWithDatetimeValuesSheet2 = WriteDatetimeColumnsIndexesFromSheetToList(datatable);

                using (ExcelPackage pck = new ExcelPackage(fileInfo))
                {
                    _worksheet = pck.Workbook.Worksheets.Add(sheetName);
                    _worksheet.Cells["A1"].LoadFromDataTable(datatable, true, GetExcelFormatAsTableStyleAccordingToReportType(report));
                    pck.Save();
                }

                ApplyNumberFormattingForDatetimeColumnsAtExcelSheet(listOfColumnIndexesWithDatetimeValuesSheet2, 2);  // For Sheet 2
            }
        }


        private void ApplyNumberFormattingForDatetimeColumnsAtExcelSheet(List<int> list, int worksheetIndex)
        {
            string fileDirPath;
            string excelColumnIndex;
            string column = "";

            Excel.Workbook workbook = null;
            Excel.Worksheet worksheet = null;

            fileDirPath = ReportPathFilenameExtension_static.ToString();
            excelColumnIndex = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            //Open the workbook at the background
            Excel._Application xlApp = new Excel.Application
            {
                Visible = false,
                DisplayAlerts = false
            };


            try
            {
                workbook = xlApp.Workbooks.Open(fileDirPath);

                worksheet = workbook.Worksheets[worksheetIndex];
                worksheet.Activate();

                foreach (var x in list)
                {
                    column = $"{excelColumnIndex[x - 1].ToString()}:{excelColumnIndex[x - 1].ToString()}"; // make string as A:A or B:B etc

                    Excel.Range overallRange = worksheet.UsedRange.Columns[column, Type.Missing].Rows;

                    overallRange.Select();
                    overallRange.NumberFormat = "dd-mm-yyyy h:mm";
                }

                worksheet.Columns.AutoFit();

                // Select cell A1 at the just for better view when opening excel file
                worksheet.get_Range("A1").Select();
            }

            finally
            {

                // Save & release Excel application object to stop the process and remove it from task manager
                workbook.Save();
                workbook.Close(true);
                xlApp.Quit();
            }
        }



        private List<int> WriteDatetimeColumnsIndexesFromSheetToList(DataTable datatable)
        {
            List<int> dateTimeColumnsIndexes = new List<int>();

            foreach (DataColumn col in datatable.Columns)
            {
                foreach (DataRow row in datatable.Rows)
                {
                    var cell = row[col.ColumnName];

                    if (cell is DateTime)
                    {
                        dateTimeColumnsIndexes.Add(col.Ordinal + 1);
                    }
                }
            }

            dateTimeColumnsIndexes = dateTimeColumnsIndexes.Distinct().ToList(); // Remove duplicates

            return dateTimeColumnsIndexes;
        }
    }



    class SupportingMethods
    {
        public void AddSheet2(TextBox tbSQL2, TextBox tbName2, Button btAdd, Button btDel)
        {
            tbSQL2.IsEnabled = true;
            tbName2.IsEnabled = true;

            btAdd.IsEnabled = false;
            btDel.IsEnabled = true;

        }

        public void DeteleSheet2(TextBox tbSQL2, TextBox tbName2, Button btAdd, Button btDel)
        {
            tbSQL2.IsEnabled = false;
            tbSQL2.Text = "";

            tbName2.IsEnabled = false;
            tbName2.Text = "";

            btAdd.IsEnabled = true;
            btDel.IsEnabled = false;

        }

        public bool Sheet2IsRequiredBeforeSetupHasBeenDone(Button btAdd, Button btDel)
        {
            bool required;


            if (btAdd.IsEnabled == false && btDel.IsEnabled == true)
            {
                required = true;
            }

            else
            {
                required = false;
            }

            return required;
        }



        public bool Sheet2IsRequiredAfterSetupHasBeenDone(string report)
        {
            bool required;

            // textboxSheetCount_REP textbox 
            ConfigTagSheet2SQL configTagSheet2SQL = new ConfigTagSheet2SQL();

            string str = configTagSheet2SQL.ReadTagContent(report);

            if (str == "")
            {
                required = false;
            }

            else
            {
                required = true;
            }

            return required;
        }
    }
}