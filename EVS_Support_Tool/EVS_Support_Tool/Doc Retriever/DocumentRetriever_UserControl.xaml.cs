﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace EVS_Support_Tool.Doc_Retriever
{
    /// <summary>
    /// Interaction logic for DocumentRetriever_UserControl.xaml
    /// </summary>
    public partial class UserControl_DocumentRetriever : UserControl
    {
        public UserControl_DocumentRetriever()
        {
            InitializeComponent();
            buttonDownload_DR.IsEnabled = false;
        }

        // Class Instances
        ButtonActionsDocRetrieval buttonActions = new ButtonActionsDocRetrieval();

        public static int currentlySelectedListBoxIndex;




        // Erase all string when clicking the caseNr textbox
        private void TextboxCaseNr_DR_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            textboxCaseNr_DR.Text = "";
        }

        private void ButtonSearch_DR_Click(object sender, RoutedEventArgs e)
        {
            //Cursor = Cursors.Wait; // Change cursor to hourglass type  
            buttonSearch_DR.IsEnabled = false;    // Blocking buttons
            textboxCaseNr_DR.IsEnabled = false;


            if (textboxCaseNr_DR.Text != "")
            {
                buttonActions.AcquireSQLDataAndFillListboxes(listBoxFilename_DR, listBoxDocument_DR, listBoxIsDeleted_DR, textboxCaseNr_DR);
            }

            buttonDownload_DR.IsEnabled = true;
            textboxCaseNr_DR.IsEnabled = false;
            buttonSearch_DR.IsEnabled = false;
        }


        private void ButtonReset_DR_Click(object sender, RoutedEventArgs e)
        {
            textboxCaseNr_DR.Text = " Replace with Case Number ...";
            listBoxFilename_DR.Items.Clear();
            listBoxDocument_DR.Items.Clear();
            listBoxIsDeleted_DR.Items.Clear();

            // Blocking Unlocking buttons
            buttonDownload_DR.IsEnabled = false;
            buttonSearch_DR.IsEnabled = true;
            textboxCaseNr_DR.IsEnabled = true;
        }


        private void ButtonDownload_DR_Click(object sender, RoutedEventArgs e)
        {
            buttonActions.GetFilenameAndExtension(listBoxFilename_DR);


        }


        // Syncronize Listboxes item lines with each other when you click at any line
        private void ListBoxFilename_DR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listBoxIsDeleted_DR.SelectedIndex = listBoxFilename_DR.SelectedIndex;
            listBoxDocument_DR.SelectedIndex = listBoxFilename_DR.SelectedIndex;

            currentlySelectedListBoxIndex = listBoxFilename_DR.SelectedIndex;
        }

        private void ListBoxDocument_DR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listBoxIsDeleted_DR.SelectedIndex = listBoxDocument_DR.SelectedIndex;
            listBoxFilename_DR.SelectedIndex = listBoxDocument_DR.SelectedIndex;

            currentlySelectedListBoxIndex = listBoxDocument_DR.SelectedIndex;
        }

        private void ListBoxIsDeleted_DR_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            listBoxDocument_DR.SelectedIndex = listBoxIsDeleted_DR.SelectedIndex;
            listBoxFilename_DR.SelectedIndex = listBoxIsDeleted_DR.SelectedIndex;

            currentlySelectedListBoxIndex = listBoxIsDeleted_DR.SelectedIndex;
        }

    }
}
