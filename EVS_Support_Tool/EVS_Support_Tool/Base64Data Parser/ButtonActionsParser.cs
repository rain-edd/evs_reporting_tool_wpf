﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace EVS_Support_Tool.Reporting_Tool
{
    /// <summary>
    /// method ReadBase64DataXMLAndLoadDataIntoRichtextbox() - used to read incoming XML and show its metadata in richtextbox
    /// method SaveDecodedBase64DataAsFileInDirectory() - saves the file from base64data xml using the tags readed from method above    
    /// </summary>
    class ButtonActionsParser
    {
        private Microsoft.Win32.OpenFileDialog _openFileDialogBase64data = new Microsoft.Win32.OpenFileDialog();
        private Microsoft.Win32.SaveFileDialog _saveFileDialogBase64data = new Microsoft.Win32.SaveFileDialog();

        private string _filename = "";
        private string _base64dataContent = "";


        public void ButtonBrowseActions(RichTextBox rtb)
        {
            // Show FileOpenDialog window and execute method if "Open" is pressed
            if (_openFileDialogBase64data.ShowDialog() == true)
            {
                ReadBase64DataXMLAndLoadDataIntoRichtextbox(rtb);
            }
        }


        public void ButtonDecodeActions()
        {
            _saveFileDialogBase64data.FileName = _filename;

            // Show FileOpenDialog window and execute method if "Open" is pressed
            if (_saveFileDialogBase64data.ShowDialog() == true)
            {
                SaveDecodedBase64DataAsFileInDirectory();
            }
        }


        public void ButtonResetActions()
        {
         
            _filename = "";
            _base64dataContent = "";
        }


        private void ReadBase64DataXMLAndLoadDataIntoRichtextbox(RichTextBox rtb)
        {
            Stream stream;
            string query;
            string connectionString_EVS45 = "Data Source=10.88.29.11\\EVS;" + "Initial Catalog=EVS45;" + "User id=SQL_eReimann;" + "Password=NPUawEE99W;";

            using (stream = _openFileDialogBase64data.OpenFile())
            {
                StreamReader reader = new StreamReader(stream);

                while (!reader.EndOfStream)
                {
                    _base64dataContent = reader.ReadLine();
                }

                string file = Path.GetFileName(_openFileDialogBase64data.FileName);
                rtb.Selection.Text = "File: " + file + Environment.NewLine + Environment.NewLine;
            }


            // Looking for docID inside the XML file between opening and closing tags
            var docId_start = _base64dataContent.IndexOf("<d2p1:_DocumentId>") + "<d2p1:_DocumentId>".Length;
            var get_docIDFromBase64 = _base64dataContent.Substring(docId_start, _base64dataContent.IndexOf("</d2p1:_DocumentId>") - docId_start);

            query = @"SELECT 
                        i.casenumber,
                        dh.*

                        FROM [EVS45].[dbo].[DocumentHeader] dh

		                left join investigationdocument idoc on idoc.DocumentId = dh.DocumentId
		                left join Investigation i on i.investigationid = idoc.investigationid

		                where dh.documentid = '" + get_docIDFromBase64 + "'";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString_EVS45))
                {
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Adding all data to Rich text box
                                rtb.AppendText("Case: " + reader.GetValue(0).ToString() + Environment.NewLine);
                                rtb.AppendText("DocumentID: " + reader.GetValue(1).ToString() + Environment.NewLine);
                                rtb.AppendText("Filename: " + reader.GetValue(4).ToString() + Environment.NewLine);
                                rtb.AppendText("Parent ref: " + reader.GetValue(18).ToString() + Environment.NewLine);
                                rtb.AppendText("Is Deleted: " + reader.GetValue(10).ToString() + Environment.NewLine);

                                _filename = reader.GetValue(4).ToString();
                            }
                        }
                    }
                }
            }

            catch (Exception LoadingErrorException)
            {
                MessageBox.Show(LoadingErrorException.Message, "An error occurred while loading SQL query", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }



        private void SaveDecodedBase64DataAsFileInDirectory()
        {
            string tempStr;

            try
            {
                var base64data_start = _base64dataContent.IndexOf("<Base64Data>") + "<Base64Data>".Length;
                var get_Base64dataFromFile = _base64dataContent.Substring(base64data_start, _base64dataContent.IndexOf("</Base64Data>") - base64data_start);


                tempStr = get_Base64dataFromFile;

                byte[] bytes = Convert.FromBase64String(tempStr);

                System.IO.FileStream stream = new FileStream(Path.GetFullPath(_saveFileDialogBase64data.FileName), FileMode.CreateNew);
                System.IO.BinaryWriter writer = new BinaryWriter(stream);

                writer.Write(bytes, 0, bytes.Length);
                writer.Close();
            }

            catch (Exception SaveFileException)
            {
                MessageBox.Show(SaveFileException.Message, "Error while saving file...", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
