﻿using System.Windows;
using System.Windows.Controls;

namespace EVS_Support_Tool.Reporting_Tool
{
    public partial class UserControl_PAR : UserControl
    {
        // Class Instances
        ButtonActionsParser buttonActionsParser = new ButtonActionsParser();

        public UserControl_PAR()
        {
            InitializeComponent();

            buttonDecode_PAR.IsEnabled = false;
        }

     //   ge daily !!! 2nd sheet            daysendom string read and use it later



        private void ButtonBrowse_PAR_Click(object sender, RoutedEventArgs e)
        {
            buttonActionsParser.ButtonBrowseActions(richtextbox_PAR);
            buttonDecode_PAR.IsEnabled = true;
        }


        private void ButtonDecode_PAR_Click(object sender, RoutedEventArgs e)
        {
            buttonActionsParser.ButtonDecodeActions();

            buttonActionsParser.ButtonResetActions(); // Call reset as well
        }


        private void ButtonReset_PAR_Click(object sender, RoutedEventArgs e)
        {
            buttonActionsParser.ButtonResetActions();
            richtextbox_PAR.Document.Blocks.Clear(); // Clear all text

            buttonDecode_PAR.IsEnabled = false;
        }
    }
}
